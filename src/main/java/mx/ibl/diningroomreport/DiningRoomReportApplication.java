package mx.ibl.diningroomreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DiningRoomReportApplication {


    public static void main(String[] args) {
        SpringApplication.run(DiningRoomReportApplication.class, args);
    }

}
