package mx.ibl.diningroomreport.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Author: Luis Enrique Cornejo Arreola
 * Job Title: Android UI Developer
 * Created At: 08 Wednesday, December 2021
 **/
public class Common {

    public static final String LARGE_DATE_TIME_FORMAT = "dd MMMM yyyy hh:mm:ss";
    public static final String SLASH_DATE_TIME_FORMAT = "MM/dd/yyyy hh:mm:ss";
    public static final String LARGE_DATE_FORMAT = "dd MMMM yyyy";
    public static final String MEDIUM_DAY_FORMAT = "dd MMMM";
    public static final String SLASH_DATE_FORMAT = "MM/dd/yyyy";
    public static final String EMAIL_DATE_FORMAT = "yyyy-MM-dd";
    public static final String TIME_FORMAT = "HH:mm:ss";

    public static String longToDateFormat( Long date, String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( format);

        return simpleDateFormat.format( new Date( date));
    }

    public static String dateToDateFormat( Date date, String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( format);

        return simpleDateFormat.format( date);
    }
}
