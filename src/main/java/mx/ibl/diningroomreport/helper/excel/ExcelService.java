package mx.ibl.diningroomreport.helper.excel;

import mx.ibl.diningroomreport.helper.Common;
import mx.ibl.diningroomreport.helper.email.Comedor;
import mx.ibl.diningroomreport.model.Register;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;


/**
 * Author: Luis Enrique Cornejo Arreola
 * Job Title: Android UI Developer
 * Created At: 09 Sunday, January 2022
 **/
@Service
public class ExcelService {

    private final JdbcTemplate jdbcTemplate;

    private static final String QUERY = "SELECT [EventDate] as date\n" +
            "      ,[UserText3] as id\n" +
            "      ,[LastName] as lastName, [FirstName] as firstName\n" +
            "\t  ,[Department] as department\n" +
            "      ,[EventLocation] as location\n" +
            "  FROM [AIUEvents_20211226053000].[dbo].[Events] As EventsQ\n" +
            "  join [AIUEvents_20211226053000].[dbo].[EventCardholders] as EventCardHoldrsQ\n" +
            "  on EventsQ.EventID = EventCardHoldrsQ.EventID\n" +
            "  join [AIUEvents_20211226053000].[dbo].[Cardholders] as CardHoldersQ\n" +
            "  on EventCardHoldrsQ.CardholderID = CardHoldersQ.CardholderID\n" +
            "  join [AIUniversal].[dbo].[Cardholders] as CardHoldersQEmp\n" +
            "  on CardHoldersQ.CardholderLink = CardHoldersQEmp.CardholderID\n" +
            "  join [AIUEvents_20211226053000].[dbo].[EventLocations] as EventLocationsQ\n" +
            "  on  EventsQ.EventLocationID =  EventLocationsQ.EventLocationID\n" +
            "  where EventsQ.EventDate > '%s 00:10:00.000'\n" +
            "\t   and EventsQ.EventDate < '%s 00:10:00.000'\n" +
            "       and EventsQ.EventDescriptionID = 4\n" +
            "\t   and EventsQ.EventLocationID = %s";

    private final int COMEDOR = 29;
    private final int COMEDOR_NUEVO = 51;
    private final int COMEDOr_VIEJO = 355;

    private XSSFWorkbook book;
    private final String EXCEL_NAME = "form_dining_room_report.xlsx";
    private final String URL_FORMAT_DOCUMENT = "/documents/";
    public final Path PATH_FORM_EXCEL_DOCUMENT = Paths.get(this.getClass().getResource(URL_FORMAT_DOCUMENT).getPath().replaceFirst("/", "")).toAbsolutePath();
    private final int ROW_INIT = 10, COLUMN_INIT = 1, COLUMN_TOTAL = 6;

    @Autowired
    public ExcelService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Genera el cuadro de asignación en un formato Excel
     *
     * @return String
     * @throws IOException In case of the file does not exist or error writing the files
     */
    public String generateDiningReport(Comedor comedor) throws IOException {
        InputStream file = new FileInputStream(PATH_FORM_EXCEL_DOCUMENT.resolve(EXCEL_NAME).toFile());
        this.book = new XSSFWorkbook(file);

        String query = null;
        String nameExcel = "";
        switch ( comedor){
            case UNO: query = getFormatQuery( COMEDOR);
                nameExcel = String.valueOf(Calendar.getInstance().getTimeInMillis()).concat("_").concat("Comedor_8.5_1_").concat(EXCEL_NAME);
            break;
            case DOS: query = getFormatQuery( COMEDOR_NUEVO);
                nameExcel = String.valueOf(Calendar.getInstance().getTimeInMillis()).concat("_").concat("Comedor 3.5_Nuevo_1_").concat(EXCEL_NAME);
            break;
            case TRES: query = getFormatQuery( COMEDOr_VIEJO);
                nameExcel = String.valueOf(Calendar.getInstance().getTimeInMillis()).concat("_").concat("Comedor 3.5_Viejo_1_").concat(EXCEL_NAME);
            break;
        }
        setRecordsInExcelFile( query);

        OutputStream fileOut = new FileOutputStream(PATH_FORM_EXCEL_DOCUMENT.resolve(nameExcel).toString());
        book.getCreationHelper().createFormulaEvaluator().evaluateAll();
        book.write(fileOut);

        // Excel input workbook close //
        fileOut.close();
        book.close();

        return nameExcel;
    }

    private String getFormatQuery(int option) {
        Calendar yesterday = Calendar.getInstance();
        yesterday.setTimeInMillis( yesterday.getTimeInMillis() - 86400000);
        Calendar today = Calendar.getInstance();

        return String.format(
                QUERY,
                Common.longToDateFormat( yesterday.getTimeInMillis(), Common.EMAIL_DATE_FORMAT),
                Common.longToDateFormat( today.getTimeInMillis(), Common.EMAIL_DATE_FORMAT),
                option
        );
    }

    private void setRecordsInExcelFile( String query) {

        List<Register> response = this.jdbcTemplate.query(
                query,
                BeanPropertyRowMapper.newInstance( Register.class)
        );

        XSSFSheet overview = this.book.getSheetAt(0);
        XSSFFont xssfFont = book.createFont();
        xssfFont.setFontName( "Arial");
        XSSFCellStyle xssfCellStyle = book.createCellStyle();
        xssfCellStyle.setFont( xssfFont);
        xssfCellStyle.setWrapText( true);

        for (int i = 0; i < response.size(); i++) {
            int row = ROW_INIT + i;

            XSSFRow xssfRow = overview.getRow( row);
            if( xssfRow == null)
                xssfRow = overview.createRow( row);

            Register register = response.get( i);

            // Set Date //
            XSSFCell xssfCell = xssfRow.getCell( COLUMN_INIT);
            if( xssfCell == null)
                xssfCell = xssfRow.createCell( COLUMN_INIT);
            xssfCell.setCellStyle( xssfCellStyle);
            xssfCell.setCellValue( Common.dateToDateFormat( register.getDate(), Common.EMAIL_DATE_FORMAT));
            // Set Hour //
            xssfCell = xssfRow.getCell( COLUMN_INIT + 1);
            if( xssfCell == null)
                xssfCell = xssfRow.createCell( COLUMN_INIT + 1);
            xssfCell.setCellStyle( xssfCellStyle);
            xssfCell.setCellValue(  Common.dateToDateFormat( register.getDate(), Common.TIME_FORMAT));
            // Set ID //
            xssfCell =  xssfRow.getCell( COLUMN_INIT + 2);
            if( xssfCell == null)
                xssfCell = xssfRow.createCell( COLUMN_INIT + 2);
            xssfCell.setCellStyle( xssfCellStyle);
            xssfCell.setCellValue( register.getId());
            // Set Employee //
            xssfCell =  xssfRow.getCell( COLUMN_INIT + 3);
            if( xssfCell == null)
                xssfCell = xssfRow.createCell( COLUMN_INIT + 3);
            xssfCell.setCellStyle( xssfCellStyle);
            xssfCell.setCellValue( register.getFirstName().concat( " ").concat( register.getLastName()));
            // Set Department //
            xssfCell =  xssfRow.getCell( COLUMN_INIT + 4);
            if( xssfCell == null)
                xssfCell = xssfRow.createCell( COLUMN_INIT + 4);
            xssfCell.setCellStyle( xssfCellStyle);
            xssfCell.setCellValue( register.getDepartment());
            // Set Location //
            xssfCell =  xssfRow.getCell( COLUMN_INIT + 5);
            if( xssfCell == null)
                xssfCell = xssfRow.createCell( COLUMN_INIT + 5);
            xssfCell.setCellStyle( xssfCellStyle);
            xssfCell.setCellValue( register.getLocation());

            // If is true means that 's the last record //
            if( ( i + 1) == response.size()){

                XSSFFont xssfFont2 = book.createFont();
                xssfFont2.setFontName( "Arial");
                XSSFCellStyle xssfCellStyle2 = book.createCellStyle();
                xssfCellStyle2.setFont( xssfFont);
                xssfCellStyle2.setWrapText( true);

                xssfRow = overview.getRow( row + 2);
                if( xssfRow == null)
                    xssfRow = overview.createRow( row + 2);
                xssfCell =  xssfRow.getCell( COLUMN_TOTAL);
                if( xssfCell == null)
                    xssfCell = xssfRow.createCell( COLUMN_TOTAL);
                xssfCell.setCellStyle( xssfCellStyle2);
                xssfCell.setCellValue( String.format( "Total comidas servidas: %s", response.size()));
            }
        }
    }
}
