package mx.ibl.diningroomreport.helper.email;

/**
 * Author: Luis Enrique Cornejo Arreola
 * Job Title: Android UI Developer
 * Created At: 09 Sunday, January 2022
 **/
public enum Comedor {
    UNO,
    DOS,
    TRES
}
