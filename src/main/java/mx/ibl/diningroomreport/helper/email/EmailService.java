package mx.ibl.diningroomreport.helper.email;

import mx.ibl.diningroomreport.helper.Common;
import mx.ibl.diningroomreport.helper.excel.ExcelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Author: Luis Enrique Cornejo Arreola
 * Job Title: Android UI Developer
 * Created At: 11 Thursday, November 2021
 **/
@Service
public class EmailService {

/*
    private final String EMAIL_ACCOUNT= "ibl.alpha@gmail.com";
    private final String PASSWORD_ACCOUNT= "dssvuyiwclzymhzx";
    private final String HOST= "smtp.gmail.com";
*/
    private final Logger logger = LoggerFactory.getLogger( EmailService.class);
    private final ExcelService excelService;
    private final String SUBJECT = "Reporte de comedores 10 AM del %s - 10 AM del %s.";

    private final String EMAIL_ACCOUNT = "lcornejo@ibl.mx";
    private final String PASSWORD_ACCOUNT = "C]I;?S24oZX2";
    private final String HOST = "mail.ibl.mx";
    private final int PORT = 587;
    private final JavaMailSender configuration;
    private final String[] emails = new String[]{ "luis.cornejo.96@hotmail.com", "luis.cornejo.2610@gmail.com"};

    @Autowired
    public EmailService(ExcelService excelService) {
        this.excelService = excelService;

        JavaMailSenderImpl config = new JavaMailSenderImpl();
        config.setHost(HOST);
        config.setPort(PORT);
        config.setUsername(EMAIL_ACCOUNT);
        config.setPassword(PASSWORD_ACCOUNT);

        Properties properties = config.getJavaMailProperties();
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.auth", true);
        properties.put("mail.smtp.starttls.enable", true);
        properties.put("mail.debug", true);

        this.configuration = config;
    }

    private String getFormatSubject() {
        Calendar yesterday = Calendar.getInstance();
        yesterday.setTimeInMillis( yesterday.getTimeInMillis() - 86400000);
        Calendar today = Calendar.getInstance();

        return String.format(
                SUBJECT,
                Common.longToDateFormat( yesterday.getTimeInMillis(), Common.MEDIUM_DAY_FORMAT),
                Common.longToDateFormat( today.getTimeInMillis(), Common.MEDIUM_DAY_FORMAT)
        );
    }

    public void sendEmail() {

        MimeMessage mime = configuration.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mime, "UTF-8");
        try {
            helper.setFrom( new InternetAddress(EMAIL_ACCOUNT));
            helper.setTo( emails);
            helper.setSubject( getFormatSubject());

            Multipart multipart = new MimeMultipart();

            // First dining report //
            MimeBodyPart attachmentPart = new MimeBodyPart();
            attachmentPart.attachFile( excelService.PATH_FORM_EXCEL_DOCUMENT.resolve(excelService.generateDiningReport( Comedor.UNO)).toFile());
            multipart.addBodyPart( attachmentPart);
            // Second dining report //
            attachmentPart = new MimeBodyPart();
            attachmentPart.attachFile( excelService.PATH_FORM_EXCEL_DOCUMENT.resolve(excelService.generateDiningReport( Comedor.DOS)).toFile());
            multipart.addBodyPart( attachmentPart);
            // Third dining report //
            attachmentPart = new MimeBodyPart();
            attachmentPart.attachFile( excelService.PATH_FORM_EXCEL_DOCUMENT.resolve(excelService.generateDiningReport( Comedor.TRES)).toFile());
            multipart.addBodyPart( attachmentPart);

            mime.setContent(multipart);
            this.configuration.send(mime);

        } catch (MessagingException | IOException me) {
            this.logger.error( "Error sending email -> {}", me.getMessage());
        }
    }
}
