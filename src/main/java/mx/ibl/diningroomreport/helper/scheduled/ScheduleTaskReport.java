package mx.ibl.diningroomreport.helper.scheduled;

import mx.ibl.diningroomreport.helper.email.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * Author: Luis Enrique Cornejo Arreola
 * Job Title: Android UI Developer
 * Created At: 09 Sunday, January 2022
 **/
@Component
public class ScheduleTaskReport {

    private final Logger logger = LoggerFactory.getLogger( ScheduleTaskReport.class);
    private final EmailService emailService;

    @Autowired
    public ScheduleTaskReport( EmailService emailService){
        this.emailService = emailService;
    }

    @Scheduled( cron = "0 30 23 * * *")
    public void sendReportByEmail(){
        logger.info( "-------------------------------------------------------------");
        logger.info( "Starting service to send reports...");
        emailService.sendEmail();
        logger.info( "Ending service to send reports...");
        logger.info( "-------------------------------------------------------------");
    }
}
