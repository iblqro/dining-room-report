package mx.ibl.diningroomreport.model;

import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * Author: Luis Enrique Cornejo Arreola
 * Job Title: Android UI Developer
 * Created At: 09 Sunday, January 2022
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Register implements Serializable {

    private String id;
    private String firstName;
    private String lastName;
    private String department;
    private String location;
    private Date date;
}
